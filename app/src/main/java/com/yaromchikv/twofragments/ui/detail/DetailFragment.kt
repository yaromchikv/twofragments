package com.yaromchikv.twofragments.ui.detail

import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionInflater
import by.kirich1409.viewbindingdelegate.viewBinding
import com.yaromchikv.twofragments.R
import com.yaromchikv.twofragments.databinding.FragmentDetailBinding
import com.yaromchikv.twofragments.ui.MainViewModel
import com.yaromchikv.twofragments.ui.overview.OverviewFragment
import kotlinx.coroutines.flow.collectLatest

class DetailFragment : Fragment(R.layout.fragment_detail) {

    private val binding by viewBinding(FragmentDetailBinding::bind)

    private val activityViewModel: MainViewModel by activityViewModels()
    private val viewModel: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        postponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(requireContext())
                .inflateTransition(android.R.transition.move)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setTransitionName(binding.image, getString(R.string.end_transition_tag))
        startPostponedEnterTransition()

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.backButtonClick()
        }

        setupCollectors()
    }

    private fun setupCollectors() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            activityViewModel.selectedItem.collectLatest { item ->
                if (item != null) {
                    with(binding) {
                        image.setBackgroundResource(R.drawable.ic_circle)
                        image.setImageResource(item.image)
                        titleText.text = item.title
                        descriptionText.text = item.description
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.events.collectLatest {
                when (it) {
                    is DetailViewModel.Event.GoBack -> {
                        activityViewModel.setCurrentFragmentName(OverviewFragment::class.simpleName)
                        activity?.supportFragmentManager?.popBackStack()
                    }
                    is DetailViewModel.Event.CloseApp -> {
                        activity?.finish()
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> viewModel.backButtonClick()
            R.id.close_app -> viewModel.closeAppButtonClick()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun newInstance() = DetailFragment()
    }
}
