package com.yaromchikv.twofragments.ui.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yaromchikv.twofragments.R
import com.yaromchikv.twofragments.databinding.ListItemBinding
import com.yaromchikv.twofragments.model.Item

class OverviewAdapter : ListAdapter<Item, OverviewAdapter.ItemViewHolder>(DIFF_CALLBACK) {

    private var onItemClickListener: ((AppCompatImageView, Item) -> Unit)? = null
    fun setOnItemClickListener(listener: (AppCompatImageView, Item) -> Unit) {
        onItemClickListener = listener
    }

    inner class ItemViewHolder(private val binding: ListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Item) {
            with(binding) {
                image.setBackgroundResource(R.drawable.ic_circle)
                image.setImageResource(item.image)
                titleText.text = item.title
                descriptionText.text = item.description

                ViewCompat.setTransitionName(image, item.id.toString())
            }

            itemView.setOnClickListener {
                onItemClickListener?.let { click -> click(binding.image, item) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ListItemBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.image == newItem.image &&
                        oldItem.title == newItem.title &&
                        oldItem.description == newItem.description
            }
        }
    }
}
