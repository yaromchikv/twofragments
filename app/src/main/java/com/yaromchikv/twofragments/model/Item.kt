package com.yaromchikv.twofragments.model

import com.yaromchikv.twofragments.R

data class Item(
    val id: Int,
    val image: Int = R.drawable.ic_code,
    val title: String,
    val description: String
)
