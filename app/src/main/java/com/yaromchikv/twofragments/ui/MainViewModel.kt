package com.yaromchikv.twofragments.ui

import androidx.lifecycle.ViewModel
import com.yaromchikv.twofragments.model.Item
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel : ViewModel() {

    private val _selectedItem = MutableStateFlow<Item?>(null)
    val selectedItem = _selectedItem.asStateFlow()

    private val _currentFragmentName = MutableStateFlow<String?>(null)
    val currentFragmentName = _currentFragmentName.asStateFlow()

    fun selectItem(item: Item) {
        _selectedItem.value = item
    }

    fun setCurrentFragmentName(name: String?) {
        _currentFragmentName.value = name
    }
}
