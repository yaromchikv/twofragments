package com.yaromchikv.twofragments.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.lifecycleScope
import com.yaromchikv.twofragments.R
import com.yaromchikv.twofragments.ui.detail.DetailFragment
import com.yaromchikv.twofragments.ui.overview.OverviewFragment
import kotlinx.coroutines.flow.collectLatest

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            showOverviewFragment()
        }
        setupCollector()
    }

    private fun showOverviewFragment() {
        viewModel.setCurrentFragmentName(OverviewFragment::class.simpleName)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, OverviewFragment.newInstance())
            .commit()
    }

    private fun setupCollector() {
        lifecycleScope.launchWhenResumed {
            viewModel.currentFragmentName.collectLatest { name ->
                when (name) {
                    OverviewFragment::class.simpleName -> supportActionBar?.hide()
                    DetailFragment::class.simpleName -> supportActionBar?.show()
                }
            }
        }
    }
}
