package com.yaromchikv.twofragments.ui.overview

import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaromchikv.twofragments.model.Item
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class OverviewViewModel : ViewModel() {

    private val _overviewUiState = MutableStateFlow<UiState>(UiState.Idle)
    val overviewUiState = _overviewUiState.asStateFlow()

    private val _events = MutableSharedFlow<Event>()
    val events = _events.asSharedFlow()

    init {
        viewModelScope.launch(Dispatchers.Default) {
            _overviewUiState.value = UiState.Loading

            val list = mutableListOf<Item>()
            for (i in 1..1000) {
                val title = "Title$i"
                val description = "Description $i"
                list.add(Item(id = i, title = title, description = description))
            }

            _overviewUiState.value = UiState.Ready(list)
        }
    }

    fun selectItemClick(imageView: AppCompatImageView, item: Item) {
        viewModelScope.launch {
            _events.emit(Event.SelectItem(imageView, item))
        }
    }

    sealed class UiState {
        data class Ready(val listOfItems: List<Item>) : UiState()
        object Loading : UiState()
        object Idle : UiState()
    }

    sealed class Event {
        data class SelectItem(val imageView: AppCompatImageView, val item: Item) : Event()
    }
}
