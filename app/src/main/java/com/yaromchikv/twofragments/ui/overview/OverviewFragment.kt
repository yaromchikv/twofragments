package com.yaromchikv.twofragments.ui.overview

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.doOnPreDraw
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.yaromchikv.twofragments.R
import com.yaromchikv.twofragments.databinding.FragmentOverviewBinding
import com.yaromchikv.twofragments.ui.MainViewModel
import com.yaromchikv.twofragments.ui.detail.DetailFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class OverviewFragment : Fragment(R.layout.fragment_overview) {

    private val binding by viewBinding(FragmentOverviewBinding::bind)

    private val activityViewModel: MainViewModel by activityViewModels()
    private val viewModel: OverviewViewModel by viewModels()

    private lateinit var overviewAdapter: OverviewAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()

        setupAdapter()
        setupCollectors()
    }

    private fun setupAdapter() {
        overviewAdapter = OverviewAdapter()

        binding.recyclerView.apply {
            adapter = overviewAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        overviewAdapter.setOnItemClickListener { imageView, item ->
            viewModel.selectItemClick(imageView, item)
        }
    }

    private fun setupCollectors() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.overviewUiState.collectLatest {
                when (it) {
                    is OverviewViewModel.UiState.Ready -> {
                        overviewAdapter.submitList(it.listOfItems)
                        binding.progressBar.isVisible = false

                        (view?.parent as? ViewGroup)?.doOnPreDraw {
                            startPostponedEnterTransition()
                        }
                    }
                    is OverviewViewModel.UiState.Loading -> {
                        binding.progressBar.isVisible = true
                    }
                    else -> Unit
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.events.collectLatest {
                when (it) {
                    is OverviewViewModel.Event.SelectItem -> {
                        activityViewModel.selectItem(it.item)
                        showDetailFragment(it.imageView)
                    }
                }
            }
        }
    }

    private fun showDetailFragment(imageView: AppCompatImageView) {
        activityViewModel.setCurrentFragmentName(DetailFragment::class.simpleName)
        activity?.supportFragmentManager?.beginTransaction()
            ?.addSharedElement(imageView, getString(R.string.end_transition_tag))
            ?.replace(R.id.fragment_container, DetailFragment.newInstance())
            ?.addToBackStack(DetailFragment::class.simpleName)
            ?.commit()
    }

    companion object {
        fun newInstance() = OverviewFragment()
    }
}
