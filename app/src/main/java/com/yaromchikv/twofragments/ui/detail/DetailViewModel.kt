package com.yaromchikv.twofragments.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class DetailViewModel : ViewModel() {

    private val _events = MutableSharedFlow<Event>()
    val events = _events.asSharedFlow()

    fun backButtonClick() {
        viewModelScope.launch {
            _events.emit(Event.GoBack)
        }
    }

    fun closeAppButtonClick() {
        viewModelScope.launch {
            _events.emit(Event.CloseApp)
        }
    }

    sealed class Event {
        object GoBack : Event()
        object CloseApp : Event()
    }
}
